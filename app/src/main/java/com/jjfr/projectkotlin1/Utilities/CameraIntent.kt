package com.jjfr.projectkotlin1.Utilities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CameraIntent(private var activity: Activity, var createFile: Boolean = true) {

    private var mCurrentPhotoPath: String? = null
    private val PERMISSION_REQUEST_CODE: Int = 101
    private val REQUEST_IMAGE_CAPTURE = 1

    private fun checkPersmission(): Boolean {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), PERMISSION_REQUEST_CODE)
    }

    fun takePicture() {
        if (checkPersmission()) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            if(intent.resolveActivity(activity.packageManager) == null)
                return

            if(createFile && isExternalStorageWritable()) {
                val file: File = createFile()

                val uri: Uri = FileProvider.getUriForFile(
                    activity,
                    "com.jjfr.projectkotlin1.fileprovider",
                    file
                )

                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            }

            activity.startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        } else {
            requestPermission()
        }
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())

        //Para almacenar internamente. En file_path.xml poner path="Android/data/com.jjfr.projectkotlin1/files/Pictures"
        //val storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_DCIM)
        //var storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        //Este codigo de abajo es lo mismo que si utilizaramos Environment.DIRECTORY_DCIM
        //Nota: A partir de Android 4.4 no se puede acceder a la sd -> https://stackoverflow.com/questions/32333094/android-fileprovider-for-ext-sdcard
        //var kk = File(Environment.getExternalStorageDirectory().path + "/DCIM")
        var storageDir: File? = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).path + "/projectKotlin1")
        storageDir?.mkdirs()
        return File.createTempFile(
            "JPEG_${timeStamp}", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
            //apply sirve para ejecutar código y poder acceder a las funciones y propiedades del objeto sobre el que se llama
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = absolutePath
        }
    }

    fun saveImageInGallery() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(mCurrentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            activity.sendBroadcast(mediaScanIntent)
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {

                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    takePicture()
                } else {
                    Toast.makeText(activity, "Permisos denegados", Toast.LENGTH_SHORT).show()
                }

                return
            }
            else -> {

            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): String? {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            if(createFile)
                saveImageInGallery()

            return mCurrentPhotoPath
        }

        return null
    }

    /* Checks if external storage is available for read and write */
    fun isExternalStorageWritable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }

    /* Checks if external storage is available to at least read */
    fun isExternalStorageReadable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state
    }
}