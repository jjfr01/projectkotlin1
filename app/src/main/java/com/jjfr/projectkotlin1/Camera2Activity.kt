package com.jjfr.projectkotlin1

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.hardware.camera2.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.Surface
import android.view.TextureView.SurfaceTextureListener
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_camera2.*
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.*
import android.hardware.camera2.CameraCharacteristics
import android.graphics.*
import android.hardware.camera2.params.StreamConfigurationMap
import android.util.Size


class Camera2Activity : AppCompatActivity() {

    //
    //CIERRO PROYECTO PORQUE YA ESTÁ HECHO Y NO VALE LA PENA SEGUIR. ADJUNTO ENLACE DEL HECHO
    //https://github.com/rnitame/Camera2BasicKotlin/blob/master/Application/src/main/kotlin/com/example/android/camera2basic/AutoFitTextureView.kt
    //HAY COSAS INTERESANTES EN ESTA CLASE COMO EL * Y CÓMO SE PASA POR PARAMETRO UNA CLASE, SEMAPHORE, LAS COROUTINES
    //

    private var isCameraOpen = false
    private lateinit var currentCameraId: String

    private var isSwitching = true

    //Creamos el semáforo con 1 permiso como máximo
    private var semaphore: Semaphore = Semaphore(1)

    private var MAX_PREVIEW_WIDTH = 1920
    private var MAX_PREVIEW_HEIGHT = 1080
    private var size = Size(0, 0)
    private var mSensorOrientation: Int? = null
    private lateinit var captureSession: CameraCaptureSession
    private lateinit var captureRequestBuilder: CaptureRequest.Builder

    private lateinit var cameraDevice: CameraDevice

    //This is used to check a camera device state. It is required to open a camera. https://proandroiddev.com/understanding-camera2-api-from-callbacks-part-1-5d348de65950
    private val deviceStateCallback = object: CameraDevice.StateCallback() {
        override fun onOpened(p0: CameraDevice) {
            cameraDevice = p0
            previewSession()
        }

        override fun onDisconnected(p0: CameraDevice) {
            p0.close()
            semaphore.release()
        }

        override fun onError(p0: CameraDevice, p1: Int) {
            //Acceder a la clase padre https://kotlinlang.org/docs/reference/this-expressions.html
            this@Camera2Activity.finish()
        }

    }

    private lateinit var backgroundThread: HandlerThread
    private lateinit var backgroundHandler: Handler

    private val PERMISSION_REQUEST_CODE = 1

    private val cameraManager by lazy {
        this.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    //TextureView is the view which renders captured camera image data. https://proandroiddev.com/understanding-camera2-api-from-callbacks-part-1-5d348de65950
    val surfaceListener = object : SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture?, p1: Int, p2: Int) {
            /*width = p1
            height = p2*/
            println("width: $p1, height: $p2")
        }

        override fun onSurfaceTextureUpdated(p0: SurfaceTexture?) {

            return
        }

        override fun onSurfaceTextureDestroyed(p0: SurfaceTexture?): Boolean {
            closeCamera()
            return true
        }

        override fun onSurfaceTextureAvailable(p0: SurfaceTexture?, p1: Int, p2: Int) {
            println("width: $p1, height: $p2")
            openCamera()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera2)

        btn_open_close_camera.setOnClickListener {
            if(isCameraOpen) {
                closeCamera()
            } else {
                openCamera()
            }
        }

        btn_change_camera.setOnClickListener {
            changeCamera()
        }
    }

    override fun onResume() {
        super.onResume()

        startBackgroundThread()
        if (tv_1.isAvailable) {
            openCamera()
        } else {
            tv_1.surfaceTextureListener = surfaceListener
        }
    }

    private fun previewSession() {
        val surfaceTexture = tv_1.surfaceTexture
        surfaceTexture.setDefaultBufferSize(MAX_PREVIEW_WIDTH, MAX_PREVIEW_HEIGHT)
        val surface = Surface(surfaceTexture)

        captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW).also {
            it.addTarget(surface)
        }

        cameraDevice.createCaptureSession(Arrays.asList(surface),
            //A callback for configuring capture sessions from a camera. This is needed to check if the camera session is configured and ready to show a preview. https://proandroiddev.com/understanding-camera2-api-from-callbacks-part-1-5d348de65950
            object: CameraCaptureSession.StateCallback() {
                override fun onConfigureFailed(p0: CameraCaptureSession) {
                    error("Creating capture session failed")
                    semaphore.release()
                }

                override fun onConfigured(p0: CameraCaptureSession) {
                    captureSession = p0
                    captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                    captureSession.setRepeatingRequest(captureRequestBuilder.build(), null, null)
                    //Cuando termine de configurar soltamos el permiso del semáforo
                    semaphore.release()
                    isSwitching = false
                }

            }, null)
    }

    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("Camera2 Kotlin").also { it.start() }
        backgroundHandler = Handler(backgroundThread.looper)
    }

    private fun stopBackgroundThread() {
        backgroundThread.quitSafely()
        try {
            backgroundThread.join()
        } catch (e: Exception) {
            error(e.toString())
        }
    }

    private fun <T> cameraCharacteristics(cameraId: String, key: CameraCharacteristics.Key<T>): T {
        val characteristics = cameraManager.getCameraCharacteristics(cameraId)
        return when (key) {
            CameraCharacteristics.LENS_FACING -> characteristics.get(key)
            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP -> characteristics.get(key)
            else -> throw IllegalArgumentException("Key not recognized")
        }
    }

    private fun getCameraId(lens: Int): String {
        var devideId: List<String>
        try {
            //Aquí se encuentran las cámaras del dispositivo (frontal, trasera...)
            val cameraIdList = cameraManager.cameraIdList
            devideId = cameraIdList.filter { lens == cameraCharacteristics(it, CameraCharacteristics.LENS_FACING) }
        } catch (e: Exception) {
            error(e.toString())
        }
        return devideId.first()
    }

    override fun onPause() {
        closeCamera()
        super.onPause()
    }

    fun openCamera() {
        startBackgroundThread()

        if (checkPersmission()) {
            connectCamera()
        } else {
            requestPermission()
        }
    }

    private fun connectCamera() {
        if(!::currentCameraId.isInitialized) {
            currentCameraId = getCameraId(CameraCharacteristics.LENS_FACING_BACK)
        }

        try {
            //Intentamos adquirid permiso del semaforo en ese tiempo y si no lo optenemos, lanzamos excepción (Esto no debería de pasar nunca ya que no tenemos más hilos que necesiten del semáforo)
            if (!semaphore.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw RuntimeException("Time out waiting to lock camera opening.")
            }

            if(checkPersmission()) {
                cameraManager.openCamera(currentCameraId, deviceStateCallback, backgroundHandler)
                isCameraOpen = true
            } else {
                requestPermission()
            }
        } catch (e: SecurityException) {
            error(e.toString())
        } catch (e: Exception) {
            error(e.toString())
        }
    }

    private fun closeCamera() {
        //Adquirimos el permiso del semáforo para ejecutar el cierre de la cámara y luego lanzamos el semáforo para que siga haciendo cosas
        semaphore.acquire()
        if(this::captureSession.isInitialized) {
            captureSession.close()
        }

        if(this::cameraDevice.isInitialized) {
            cameraDevice.close()
        }

        stopBackgroundThread()

        isCameraOpen = false
        //Lanzamos el permiso del semáforo
        semaphore.release()
    }

    private fun checkPersmission(): Boolean {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {

                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                ) {
                    openCamera()
                } else {
                    Toast.makeText(this, "Permisos denegados", Toast.LENGTH_SHORT).show()
                }

                return
            }
            else -> {

            }
        }
    }

    private fun changeCamera() {
        if(!isSwitching) {
            isSwitching = true

            GlobalScope.launch { // launch a new coroutine in background and continue
                cameraManager.cameraIdList.find { x -> x != currentCameraId }?.also { currentCameraId = it }

                closeCamera()
                openCamera()
            }
        }
    }

    private fun setUpCameraOutputs(width: Int, height: Int) {

        /*try {*/
            val characteristics = cameraManager.getCameraCharacteristics(currentCameraId)
            var map: StreamConfigurationMap = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!

            //* -> https://stackoverflow.com/questions/39389003/kotlin-asterisk-operator-before-variable-name-or-spread-operator-in-kotlin
            var largest: Size = Collections.max(
                    Arrays.asList(*map.getOutputSizes(ImageFormat.JPEG)),
                    CompareSizesByArea())

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            var displayRotation: Int = windowManager.defaultDisplay.rotation

            //noinspection ConstantConditions
            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION)

            var swappedDimensions = false
            when (displayRotation) {
                Surface.ROTATION_0, Surface.ROTATION_180 -> if (mSensorOrientation === 90 || mSensorOrientation === 270) {
                    swappedDimensions = true
                }
                Surface.ROTATION_90, Surface.ROTATION_270 -> if (mSensorOrientation === 0 || mSensorOrientation === 180) {
                    swappedDimensions = true
                }
                else -> println("Display rotation is invalid: $displayRotation")
            }

            var displaySize: Point = Point()
            windowManager.defaultDisplay.getSize(displaySize)
            var rotatedPreviewWidth: Int = width
            var rotatedPreviewHeight: Int = height
            var maxPreviewWidth: Int = displaySize.x
            var maxPreviewHeight: Int = displaySize.y

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
            // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
            // garbage capture data.
            size = chooseOptimalSize(map.getOutputSizes<SurfaceTexture>(SurfaceTexture::class.java),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, largest)


        /*} catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        }*/
    }

    /**
     * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private fun configureTransform(viewWidth: Int, viewHeight: Int) {
        val rotation = windowManager.defaultDisplay.rotation
        val matrix = Matrix()
        val viewRect = RectF(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
        val bufferRect = RectF(0f, 0f, size.height.toFloat(), size.width.toFloat())
        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
            val scale = Math.max(
                viewHeight.toFloat() / size.height,
                viewWidth.toFloat() / size.width
            )
            matrix.postScale(scale, scale, centerX, centerY)
            matrix.postRotate(90 * (rotation - 2).toFloat(), centerX, centerY)
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180f, centerX, centerY)
        }
        tv_1.setTransform(matrix)
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private fun chooseOptimalSize(
        choices: Array<Size>, textureViewWidth: Int,
        textureViewHeight: Int, maxWidth: Int, maxHeight: Int, aspectRatio: Size): Size {

        // Collect the supported resolutions that are at least as big as the preview Surface
        val bigEnough: MutableList <Size> = mutableListOf()
        // Collect the supported resolutions that are smaller than the preview Surface
        val notBigEnough: MutableList <Size> = mutableListOf(Size(aspectRatio.width, aspectRatio.height))
        val w: Int = aspectRatio.width
        val h: Int = aspectRatio.height

        choices.forEach {
            option ->
            if(option.width <= maxWidth && option.height <= maxHeight && option.height == option.width * h / w) {
                if(option.width >= textureViewWidth && option.height >= textureViewHeight) {
                    bigEnough.add(option)
                } else {
                    notBigEnough.add(option)
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size > 0) {
            return Collections.min(bigEnough, CompareSizesByArea())
        } else if (notBigEnough.size > 0) {
            return Collections.max(notBigEnough, CompareSizesByArea())
        } else {
            println("Couldn't find any suitable preview size")
            return choices[0]
        }
    }

    /**
     * Compares two `Size`s based on their areas.
     */
    internal class CompareSizesByArea : Comparator<Size> {

        override fun compare(lhs: Size, rhs: Size): Int {
            // We cast here to ensure the multiplications won't overflow
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height - rhs.width.toLong() * rhs.height)
        }

    }

}
