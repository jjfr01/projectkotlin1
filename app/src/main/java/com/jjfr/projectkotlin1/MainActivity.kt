package com.jjfr.projectkotlin1

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jjfr.projectkotlin1.Utilities.CameraIntent

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //Las variables que no puede ser nullables deben inicializarse pero con lateinit puedes posponer esa inicialización
    private val camera = CameraIntent(this)

    //Modelo rápido (No hace falta declarar get ni set(los set se autoimplentan si las variables son var)
    data class Prueba(val name: String)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Declaración de variable (inmutable -> val ; mutable -> var)
        val prueba = Prueba("Hola")

        val kk: (String) -> Unit = { x ->
            println(x)
        }

        prueba.name.also(kk)

        btn_capture.setOnClickListener {
            camera.takePicture()
        }

        btn_camera2.setOnClickListener {
            val intent = Intent(this, Camera2Activity::class.java)

            startActivity(intent)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        camera.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        camera.onActivityResult(requestCode, resultCode, data)?.also { result ->

            val ei = ExifInterface(result)

            val orientation = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            var bitmap: Bitmap = BitmapFactory.decodeFile(result)

            var rotation = when (orientation) {

                ExifInterface.ORIENTATION_ROTATE_90 -> 90f

                ExifInterface.ORIENTATION_ROTATE_180 -> 180f

                ExifInterface.ORIENTATION_ROTATE_270 -> 270f

                ExifInterface.ORIENTATION_NORMAL -> 0f
                else -> 0f
            }

            bitmap = Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                Matrix().apply { postRotate(rotation) },
                true
            )
            img_1.setImageBitmap(bitmap)
        }
    }

}
